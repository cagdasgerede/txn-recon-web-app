Welcome to Transaction Analyzer!
===================

# Notes:


 - Heroku address: https://txnreconciler.herokuapp.com/
 
 - Demo Video:
 [![Demo Video](https://i.imgur.com/nfXlxFs.png)](https://www.youtube.com/watch?v=p_Whsc7tGoU "Demo Video")


 - Bitbucket address: https://bitbucket.org/cagdasgerede/txn-recon-web-app


 - About me: https://stackoverflow.com/users/story/626563



I developed the project and deployed to heroku. The source code is available
as a git repository in bitbucket. I wrote unit tests to test the matching
library I wrote and the current test coverage is 91%). I also worked on exception
handling and proper error reporting. Please let me know if you have any questions.



# About the application

This is a web application to reconcile two sets of transactions. The transactions are uploaded in CSV files where each line contains a comma separated value representing a transaction.

There are 4 possible results for each transaction:

1. It perfectly matches with a transaction from the other set: In this
    case, these transactions do not need any human inspection;
    therefore, we ignore them.
    
2. It almost perfectly matches with a transaction from the other set: For this, we apply some heuristics to understand what type of variations can be ignored and do not need human inspection. For this, currently we use the following approaches:

 - When comparing the transaction fields with text values (ProfileName, TransactionDescription, TransactionNarrative), we ignore casing (i.e., we use equalsIgnoreCase).

  - When comparing TransactionDescription and TransactionNarrative, we compute the edit distance and if the edit distance is below certain threshold, we ignore the variation. This happens: i) when a word is mistyped
(GABORONE vs. GABERONE); ii) when a word is abbreviated (BWS vs. BOTSWANA); iii) vowels are skipped (NORTHGATE vs. NRTHGT); iv) statement is cut short. The threshold value is calculated based on consonent-to-vowel ratio. Currently, the ratio is set to English language's rate.

 - When comparing TransactionDate fields, we try to check if the variations may happen due to small clock differences and timezone differences in recording locations. For this, we use two heuristics. First, if the time
difference is less than 2 minutes, then we consider this an ignorable clock difference (2017-09-20 10:20:50 vs. 2017-09-20 10:21:10). Second, if the time difference is roughy a whole number multiple of an hour, then we assume this is due to a timezone difference (2017-09-20 10:20:50 vs. 2017-09-20 14:21:10).


3. It does not match with any transaction. We report this for human inspection.
4. It matches with a transaction if we ignore "enough" number of fields. We also report this match for human inspection.






# Assumptions
I assume the transactions can fit in memory. The example files are about 40KB for which there is no issue. For very large files, a different approach needs to be applied (see the section "Scaling to work with larger files").

I also assume the transaction file format and the fields are fixed. I make assumptions about certain fields (such TransactionDate, TransactionNarrative, TransactionDescription) to develop heuristics.

When there are duplicate transactions in the same file, I assumed they can be ignored. If I had two transactions in file 1 and one transaction in file 2, and they all match, I reported all as perfect matches. Alternatively, I could
report a single pair as a match, and report the third transaction as an unmatch. It was not clear which one was more desirable, I went ahead with the former approach.

I assume that most of the transactions are going to be perfect matches. In the example files, the number of unmatching transactions is less than 10%. Therefore, I assume 90% of the transactions are going to be eliminated in the first phase of the algorithm. In the Step 2 of my algorithm, I use an algorithm that takes quadratic time (see the section Time Complexity Analysis) but this should not be a problem if my assumption is reasonable in practice (For the example files, we only have 16 transactions left to process for this step).


# Algorithm

 - Step 0: Load all transactions into memory
 - Step 1: Eliminate perfectly matching transactions
 - Step 2: Eliminate almost-perfectly matching transactions (uses a few heuristics)
 - Step 3: Align remaining transactions for human inspection
 - Step 4: Prepare a view for the results

# Time Complexity Analysis

- Step 0: Linear: reading both files and inserting the data to lists)

- Step 1: Linear: put data to a set data structure and then use set membership
check to find perfect matches (There are N lookups where N is the number transactions. Each lookup takes O(1) time. On the other hand, the lookup operation works on String characters for comparisons; therefore, it would be more accurate to say that each lookup takes O(w) where w is the length of a transaction record. Then, we must say the overall complexity of this step is quadratic, i.e., O(N*w). I assume the transaction length is negligible compared to the number of transactions; so for all practical purposes, we can say this
step is linear.

- Step 2: Quadratic: For each possible pair, we compute an edit distance. There are O(N square) such pairs at most where N is the number of transactions in one file. For edit distance, we use Levenshtein Distance
(https://en.wikipedia.org/wiki/Levenshtein_distance). Each distance computation takes quadratic time in the size of the strings. Overall, we can say the time complexity is O(N^2 * w^2). I assume the transaction length is negligible compared to the number of transactions; so for all practical purposes, we can say this step is quadratic.

- Step 3: Quadratic: Similar to the previous step.

- Step 4: Linear


# How to run the web application

You can try out the application at:  https://txnreconciler.herokuapp.com/

If you would like to build and run in your machine then execute the following commands (you need maven https://maven.apache.org/). :

```
mvn package
java -cp target/my-app-1.0-jar-with-dependencies.jar Main
```

Then open the browser and go to localhost (The application tries to bind to the port 8080). If this port is being used, go to src/main/java/Main.java and modify "return 8080" line. Alternatively, if you have heroku installed in your system, you can run:

```
heroku local
```


# Potential improvements over the current implementation

## More heuristics for detecting ignoreable variations between transactions

TransactionNarrative field usually contains an address. It ends with the city
name and the country name. When comparing this field, city abbreviations could
be taken into consideration. For example, the following variations appear in the
example transaction files.

```
PAL vs. PALAPYE
BWS vs. BW vs. BOTSWANA vs. BOT
FRA vs. FRANCISTOWN
```

We could leverage this idea when computing the edit distance.

## Scaling to work with larger files
Currently, the content of the uploaded files are taken into the memory. If the
file contents are larger than a few hundred megabytes (each would contain
more than a couple of million transactions), then the current approach would
not scale. For such cases, the uploaded file can be saved to a temporary file
and then the computation could be done by reading chunks of data into memory
similar to external sorting (https://en.wikipedia.org/wiki/External_sorting).

Another problem that would occur in such cases is that uploading may take
a very long time. If something goes wrong with the network or the connection to
the server, the upload process may fail. Then. the user would have to start
the upload process all over again. To improve this, we could change the upload
functionality on the server side so that the user can pause or resume the upload
process. Another possibility would be to provide a separate mechanism such as
ftp and scp for uploading files.

Finally, if we have a very large amount of unmatched transactions, Velocity
Template Engine may cause JVM to go out of memory while producing the large
view. One solution to this would be to produce the view in chunks and provide
pagination (render only x transaction pairs per page).

## Improving usability

The application's current user interface could use some designer's help to
improve the styling.

Moreover, currently the server does not remember any past analysis. Therefore,
the same files have to be uploaded again each time the analysis need to be
redone. For small files, this is not a big concern, since upload process
usually takes less than a second.

The application does not provide a way to export the analysis results.
Simply printing the browser page might be enough but in case more detailed
information is needed for each transaction pair, then some more work could be
done to improve this part.


#Technologies used to build the web application

- Spark: sparkjava.com
- Velocity Template Engine: http://velocity.apache.org/
- Apache's text similarity implementation: https://commons.apache.org/proper/commons-text/apidocs/org/apache/commons/text/similarity/LevenshteinDistance.html


# Generate test coverage

Currently we have 91% branch coverage. To generate the coverage report, we use jacoco.

[![Branch Coverage](https://i.imgur.com/2ibiFtk.png)](https://i.imgur.com/2ibiFtk.png "Branch Coverage")



Execute the following commands to generate the report:

```
mvn clean test jacoco:report
```

Then open the following file in your browser to view the coverage results:

```
target/site/jacoco/index.html 
```


# Generate maven project reports

```
mvn site
```

Then open:

```
target/site/index.html
```

