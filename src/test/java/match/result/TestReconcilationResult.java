package match.result;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestReconcilationResult {
	ReconcilationResult result;
	MatchCounts counts1, counts2;
	ArrayList<TransactionPair> pairs;

	@Before
	public void setUp() {
		counts1 = new MatchCounts(5, 4);
		counts2 = new MatchCounts(3, 2);
		pairs = new ArrayList<TransactionPair>();
		result = new ReconcilationResult(counts1, counts2, pairs);
	}

	@Test
	public void testGetFileOneCounts() {
		assertEquals(counts1, result.getFileOneCounts());
	}

	@Test
	public void testGetFileTwoCounts() {
		assertEquals(counts2, result.getFileTwoCounts());
	}

	@Test
	public void testGetApproximatelyMatchingPairs() {
		assertEquals(pairs, result.getApproximatelyMatchingPairs());
	}
}