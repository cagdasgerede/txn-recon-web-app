package match.result;

import java.util.ArrayList;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

public class TestTransaction {
	@Test
	public void testBuild() {
		String txnLine =
			"Card Campaign," +  // profile name
			"2014-01-11 22:27:44," +  // date
			"-20000," +  // amount
			"*MOLEPS ATM25             MOLEPOLOLE    BW," +  // narrative
			"DEDUCT," + // description
			"0584011808649511," +  // id
			"1," +  // type
			"P_NzI2ODY2ODlfMTM4MjcwMTU2NS45MzA5,";  // wallet reference
		Transaction txn = Transaction.build(txnLine);

		assertEquals("Card Campaign", txn.getProfileName());
		assertEquals("2014-01-11 22:27:44", txn.getTransactionDate());
		assertEquals("-20000", txn.getTransactionAmount());
		assertEquals("*MOLEPS ATM25             MOLEPOLOLE    BW",
					 txn.getTransactionNarrative());
		assertEquals("DEDUCT", txn.getTransactionDescription());
		assertEquals("0584011808649511", txn.getTransactionID());
		assertEquals("1", txn.getTransactionType());
		assertEquals("P_NzI2ODY2ODlfMTM4MjcwMTU2NS45MzA5",
					 txn.getWalletReference());
		assertEquals(txnLine, txn.toString());
	}

	@Test
	public void testComputeDistance() throws TransactionFormatException {
		TransactionScaffold txn = new TransactionScaffold();
		txn.setProfileName("A");
		txn.setTransactionAmount("100");
		txn.setTransactionID("12345");
		txn.setTransactionType("1");
		txn.setWalletReference("P_abcde");

		assertEquals(0, txn.computeDistance(txn));

		// Distance function is symmetric.
		assertEquals(5, txn.computeDistance(Transaction.EMPTY));
		assertEquals(5, Transaction.EMPTY.computeDistance(txn));

		Transaction anotherTxn = new TransactionScaffold();
		txn.setProfileName("B");
		txn.setTransactionAmount("200");
		txn.setTransactionID("67890");
		txn.setTransactionType("0");
		txn.setWalletReference("P_fghij");        
		assertEquals(5, txn.computeDistance(anotherTxn));
	}

	@Test
	public void testComputeDistanceIgnoresCaseForTextFields()
	throws TransactionFormatException  {
		String profileName = "Card Campaign";
		String upperCaseProfileName = "CARD CAMPAIGN";
		String narrative = "*NORTHGATE MALL GABORONE BW";
		String lowerCaseNarrative = "*northgate mall gaborone bw";
		String description = "deduct";
		String upperCaseDescription = "DEDUCT";

		TransactionScaffold txn = new TransactionScaffold();
		txn.setProfileName(profileName);
		txn.setTransactionNarrative(narrative);
		txn.setTransactionDescription(description);

		TransactionScaffold anotherTxn = new TransactionScaffold();
		anotherTxn.setProfileName(upperCaseProfileName);
		anotherTxn.setTransactionNarrative(lowerCaseNarrative);
		anotherTxn.setTransactionDescription(upperCaseDescription);

		assertEquals(0, txn.computeDistance(anotherTxn));
	}

	@Test
	public void testComputeDistanceSimilarNarrative()
	throws TransactionFormatException {
		TransactionScaffold txn = new TransactionScaffold();
		txn.setTransactionNarrative("*NORTHGATE MALL GABORONE BW");

		TransactionScaffold anotherTxn = new TransactionScaffold();
		anotherTxn.setTransactionNarrative("*NRTHGT MLL GBRN BW");

		assertEquals(0, txn.computeDistance(anotherTxn));
	}

	@Test
	public void testComputeDistanceNotSimilarNarrative()
	throws TransactionFormatException {
		TransactionScaffold txn = new TransactionScaffold();
		txn.setTransactionNarrative("*NORTHGATE MALL GABORONE BW");

		TransactionScaffold yetAnotherTxn = new TransactionScaffold();
		yetAnotherTxn.setTransactionNarrative(
		"*NRTHGT MLL GBRN BW TOO MUCH DIFFERENCE");

		assertEquals(1, txn.computeDistance(yetAnotherTxn));
	}

	@Test
	public void testComputeDistanceSimilarDescription()
	throws TransactionFormatException {
		TransactionScaffold txn = new TransactionScaffold();
		txn.setTransactionNarrative("DEDUCT");

		TransactionScaffold anotherTxn = new TransactionScaffold();
		anotherTxn.setTransactionNarrative("DEDCT");

		assertEquals(0, txn.computeDistance(anotherTxn));
	}

	@Test
	public void testComputeDistanceNotSimilarDescription()
	throws TransactionFormatException {
		TransactionScaffold txn = new TransactionScaffold();
		txn.setTransactionNarrative("DEDUCT");

		TransactionScaffold anotherTxn = new TransactionScaffold();
		anotherTxn.setTransactionNarrative("REVERSE");

		assertEquals(1, txn.computeDistance(anotherTxn));
	}

	@Test
	public void testComputeDistanceSimilarDate()
	throws TransactionFormatException {
		TransactionScaffold txn = new TransactionScaffold();
		txn.setTransactionDate("2014-01-12 11:11:09");

		TransactionScaffold anotherTxn = new TransactionScaffold();
		anotherTxn.setTransactionDate("2014-01-13 11:11:09");
		assertEquals(0, txn.computeDistance(anotherTxn));
        
		TransactionScaffold yetAnotherTxn = new TransactionScaffold();
		yetAnotherTxn.setTransactionDate("2014-01-12 11:11:39");
		assertEquals(0, txn.computeDistance(yetAnotherTxn));

		TransactionScaffold someTxn = new TransactionScaffold();
		someTxn.setTransactionDate("2014-01-12 15:10:25");
		assertEquals(0, txn.computeDistance(someTxn));
	}

	@Test
	public void testComputeDistanceNotSimilarDate()
	throws TransactionFormatException {
		TransactionScaffold txn = new TransactionScaffold();
		txn.setTransactionDate("2014-01-12 11:11:09");

		TransactionScaffold anotherTxn = new TransactionScaffold();
		anotherTxn.setTransactionDate("2014-01-13 11:31:09");
		assertEquals(1, txn.computeDistance(anotherTxn));
        
		TransactionScaffold yetAnotherTxn = new TransactionScaffold();
		yetAnotherTxn.setTransactionDate("2014-01-12 11:15:09");
		assertEquals(1, txn.computeDistance(yetAnotherTxn));
	}

	@Test
	public void testComputeDistanceThrowsDateFormatException() {
		try {
			TransactionScaffold txn = new TransactionScaffold();
			txn.setAsString("some transaction");
			txn.setTransactionDate("incorrect format");

			TransactionScaffold anotherTxn = new TransactionScaffold();
			anotherTxn.setAsString("2014-01-12 11:15:09");

			txn.computeDistance(anotherTxn);
			fail();
		} catch(TransactionFormatException e) {
			assertEquals(
				"The TransactionDate format is incorrect.",
				e.getErrorMessage());
			assertEquals("some transaction", e.getBadlyFormattedData());
		}
	}

	/**
	 * A convenience class for testing which provides methods to configure a
	 * transaction. 
	 **/
	class TransactionScaffold extends Transaction {
		public void setProfileName(String profileName) {
			this.profileName = profileName;
		}

		public void setTransactionDate(String transactionDate) {
			this.transactionDate =  transactionDate;
		}

		public void setTransactionAmount(String transactionAmount) {
			this.transactionAmount = transactionAmount;
		}

		public void setTransactionNarrative(String transactionNarrative) {
			this.transactionNarrative = transactionNarrative;
		}

		public void setTransactionDescription(String transactionDescription) {
			this.transactionDescription = transactionDescription;
		}

		public void setTransactionID(String transactionID) {
			this.transactionID = transactionID;
		}

		public void setTransactionType(String transactionType) {
			this.transactionType = transactionType;
		}

		public void setWalletReference(String walletReference) {
			this.walletReference = walletReference;
		}

		public void setAsString(String asString) {
			this.asString = asString;
		}
	}  // class TransactionScaffold
}  // class TestTransaction