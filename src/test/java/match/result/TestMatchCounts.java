package match.result;

import org.junit.Test;

import static junit.framework.Assert.*;

public class TestMatchCounts {
	final static int TOTAL = 5;
	final static int UNMATCHING = 3;
	MatchCounts counts = new MatchCounts(TOTAL, UNMATCHING);

	@Test
	public void testGetUnmatchingTransactionCount() {
		assertEquals(UNMATCHING, counts.getUnmatchingTransactionCount());
	}

	@Test
	public void testGetMatchingTransactionCount() {
		assertEquals(TOTAL - UNMATCHING, counts.getMatchingTransactionCount());
	}

	@Test
	public void testGetTotalTransactionCount() {
		assertEquals(TOTAL, counts.getTotalTransactionCount());
	}
}