package match.util;

import match.result.TransactionFormatException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class TestDateUtil {

	@Test
	public void testCloseEnoughWorksForClockVariations() {
		assertTrue(DateUtil.closeEnough(
			DateUtil.getDate("2017-09-20 17:30:58"),
			DateUtil.getDate("2017-09-20 17:31:40")));
	}

	@Test
	public void testCloseEnoughWorksForTimeZoneDifferences() {
		assertTrue(DateUtil.closeEnough(
			DateUtil.getDate("2017-09-20 17:30:58"),
			DateUtil.getDate("2017-09-20 14:31:40")));
	}

	@Test
	public void testCloseEnoughWorksWhenTimesAreDifferent() {
		assertFalse(DateUtil.closeEnough(
			DateUtil.getDate("2017-09-20 17:30:58"),
			DateUtil.getDate("2017-09-22 17:30:58")));

		assertFalse(DateUtil.closeEnough(
			DateUtil.getDate("2017-09-20 17:30:10"),
			DateUtil.getDate("2017-09-20 17:45:20")));
	}

	@Test
	public void testGetDate() {
		LocalDateTime dt = DateUtil.getDate("2017-09-20 17:30:58");
		assertEquals(2017, dt.getYear());
		assertEquals(9, dt.getMonthValue());
		assertEquals(20, dt.getDayOfMonth());
		assertEquals(17, dt.getHour());
		assertEquals(30, dt.getMinute());
		assertEquals(58, dt.getSecond());
	}

	@Test(expected = DateTimeParseException.class)
	public void testGetDateThrowsExceptionWhenFormatIsWrong() {
		DateUtil.getDate("2017-09-20 17-30-58");
	}
}
