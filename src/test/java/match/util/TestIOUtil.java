package match.util;

import match.result.TransactionFormatException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

public class TestIOUtil {

	@Test
	public void testReadTransactions()
	throws IOException, TransactionFormatException {
		String header =
			"ProfileName,TransactionDate,TransactionAmount," +
			"TransactionNarrative,TransactionDescription," +
			"TransactionID,TransactionType,WalletReference";
		String txn =
			"Card Campaign,2014-01-11 22:27:44,-20000," +
			"*MOLEPS ATM25             MOLEPOLOLE    BW,DEDUCT,"+
			"0584011808649511,1,P_NzI2ODY2ODlfMTM4MjcwMTU2NS45MzA5,";
		String concatenated = header + "\n" + txn;
		BufferedReader reader = new BufferedReader(
			new InputStreamReader(
				new ByteArrayInputStream(concatenated.getBytes())));

		ArrayList<String> txns = IOUtil.readTransactions(reader);
		assertEquals(1, txns.size());
		assertEquals(txn, txns.get(0));
	}

	@Test
	public void testReadTransactionsThrowsExceptionWhenHeaderIsMissing()
	throws IOException, TransactionFormatException {
		String header = "bogus header";
		String txn =
			"Card Campaign,2014-01-11 22:27:44,-20000," +
				"*MOLEPS ATM25             MOLEPOLOLE    BW,DEDUCT,"+
				"0584011808649511,1,P_NzI2ODY2ODlfMTM4MjcwMTU2NS45MzA5,";
		String concatenated = header + "\n" + txn;
		BufferedReader reader = new BufferedReader(
			new InputStreamReader(
				new ByteArrayInputStream(concatenated.getBytes())));

		try {
			IOUtil.readTransactions(reader);
			fail();
		} catch(TransactionFormatException e) {
			assertEquals(header, e.getBadlyFormattedData());
		}
	}

	@Test
	public void testReadTransactionsThrowsExceptionWhenHeaderIsIncorrect()
	throws IOException, TransactionFormatException {
		String header =
			"ProfileName,TransactionDate," +
			"INCORRECT," +  // bogus header part
			"TransactionNarrative,TransactionDescription," +
			"TransactionID,TransactionType,WalletReference";
		String txn =
			"Card Campaign,2014-01-11 22:27:44,-20000," +
			"*MOLEPS ATM25             MOLEPOLOLE    BW,DEDUCT,"+
			"0584011808649511,1,P_NzI2ODY2ODlfMTM4MjcwMTU2NS45MzA5,";
		String concatenated = header + "\n" + txn;
		BufferedReader reader = new BufferedReader(
			new InputStreamReader(
				new ByteArrayInputStream(concatenated.getBytes())));

		try {
			IOUtil.readTransactions(reader);
			fail();
		} catch(TransactionFormatException e) {
			assertEquals(header, e.getBadlyFormattedData());
		}
	}
}
