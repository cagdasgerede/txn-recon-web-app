package match.result;

import match.Reconcile;
import match.result.TransactionFormatException;
import match.util.IOUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestReconcile {

	@Test
	public void testReconcileEndToEnd()
	throws IOException, TransactionFormatException {
		String file1 = new File(
			"src/test/data/ClientMarkoffFile20140113.csv").getAbsolutePath();
		String file2 = new File(
			"src/test/data/TutukaMarkoffFile20140113.csv").getAbsolutePath();

		ArrayList<String> transactionsA = IOUtil.readTransactions(file1);
		ArrayList<String> transactionsB = IOUtil.readTransactions(file2);

		ReconcilationResult result =
			Reconcile.run(transactionsA, transactionsB, 2);

		MatchCounts counts = result.getFileOneCounts();
		assertEquals(12, counts.getUnmatchingTransactionCount());
		assertEquals(294, counts.getMatchingTransactionCount());
		assertEquals(306, counts.getTotalTransactionCount());

		counts = result.getFileTwoCounts();
		assertEquals(13, counts.getUnmatchingTransactionCount());
		assertEquals(292, counts.getMatchingTransactionCount());
		assertEquals(305, counts.getTotalTransactionCount());

		ArrayList<TransactionPair> pairs =
			result.getApproximatelyMatchingPairs();
		int danglingCount = 0;
		for (TransactionPair p : pairs) {
			if (p.getLeft() == Transaction.EMPTY ||
					p.getRight() == Transaction.EMPTY) {
				danglingCount++;
			}
		}
		assertEquals(6, danglingCount);
	}
}
