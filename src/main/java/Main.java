import match.Reconcile;
import match.result.ReconcilationResult;
import match.result.TransactionFormatException;
import match.util.IOUtil;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;

import spark.Request;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.staticFileLocation;

import static spark.debug.DebugScreen.enableDebugScreen;

/**
 * A simple web application that provides functionality to align two sets
 * of transactions. The uploaded files are expected to be CSV files.
 * The each row in a CSV file must contain comma separated values. The first row
 * must contain the headers. The expected headers are (in the given order):
 * 
 * <ul>
 *  <li> ProfileName
 *  <li> TransactionDate
 *  <li> TransactionAmount
 *  <li> TransactionNarrative
 *  <li> TransactionDescription
 *  <li> TransactionID
 *  <li> TransactionType
 *  <li> WalletReference
 * </ul>
 * 
 * <p>
 * At the end of the alignment, the numbers of exact matches and non-matches
 * are reported. For every transaction without any perfect match, an approximate
 * matching transaction is searched for. If found, such alignments are reported
 * in detail.
 *
 * The application is developed using the Spark micro framework. For the template
 * engine, it uses the Velocity Template Engine. The project uses maven for
 * build and dependency management. It is also configured to be deployable to
 * heroku. For futher instructions, checkout the README.md file.
 */
public class Main {
    public final static String
        UPLOADED_FILE_1_PART_NAME = "uploaded_file1",
        UPLOADED_FILE_2_PART_NAME = "uploaded_file2",
        DISTANCE_THRESHOLD_PART_NAME = "distance_threshold";

    public static void main(String[] args) {
        port(getHerokuAssignedPort());
        enableDebugScreen();
        staticFileLocation("/public");

        get("/", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            return render(model, "templates/layout.vtl");
        });

        post("/", (req, res) -> {
            long startTimestamp = System.currentTimeMillis();
            req.attribute("org.eclipse.jetty.multipartConfig",
                          new MultipartConfigElement("/temp"));

            int distanceThreshold = 2;
            String distanceThresholdAsString =
                req.queryParams(DISTANCE_THRESHOLD_PART_NAME);
            try {
                distanceThreshold = Integer.parseInt(distanceThresholdAsString);
            } catch (NumberFormatException e) {
                Map<String, Object> model = new HashMap<>();
                model.put("error_title", "Analysis failed");
                model.put(
                    "error_message",
                    "You did not enter a valid number for the threshold.");
                model.put(
                    "error_cause",
                    "The value entered was " + distanceThresholdAsString);
                return render(model, "templates/layout.vtl");
            }

            ArrayList<String> fileOneTxns, fileTwoTxns;
            try (
                BufferedReader reader1 =
                    getBufferedReader(req, UPLOADED_FILE_1_PART_NAME);
                BufferedReader reader2 =
                    getBufferedReader(req, UPLOADED_FILE_2_PART_NAME);
            ) {
                fileOneTxns = IOUtil.readTransactions(reader1);
                fileTwoTxns = IOUtil.readTransactions(reader2);
            } catch(TransactionFormatException e) {
                return getRenderModelForException(e);
            } catch(IOException e) {
                Map<String, Object> model = new HashMap<>();
                model.put("error_title", "Something went wrong with file IO.");
                model.put(
                    "error_message",
                    "The error message: " + e.getMessage());
                return render(model, "templates/layout.vtl");
            }

            ReconcilationResult result;
            try {
                result = Reconcile.run(
                    fileOneTxns,
                    fileTwoTxns,
                    distanceThreshold);
            } catch (TransactionFormatException e) {
                return getRenderModelForException(e);
            }

            return renderResult(req, result, startTimestamp);
        });
    } // main

    private static String getRenderModelForException(TransactionFormatException e) {
        Map<String, Object> model = new HashMap<>();
        model.put("error_title", "Found improperly formatted transaction");
        model.put("error_message", "The error message: " + e.getErrorMessage());
        model.put("error_cause",
                  "The problematic line: " + e.getBadlyFormattedData());
        return render(model, "templates/layout.vtl");
    }

    private static String renderResult(
            Request req, ReconcilationResult result, long startTimestamp) {
        String file1Name, file2Name;
        try {
            file1Name = getFileName(
                req.raw().getPart(UPLOADED_FILE_1_PART_NAME));
            file2Name = getFileName(
                req.raw().getPart(UPLOADED_FILE_2_PART_NAME));
        } catch(IOException e) {
            return renderInternalError(
                e, "Could not retrieve the uploaded file name");
        } catch(ServletException e) {
            return renderInternalError(e, "");
        }

        Map<String, Object> model = new HashMap<>();
        model.put("result", result);
        model.put("fileOneName", file1Name);
        model.put("fileTwoName", file2Name);
        long duration = System.currentTimeMillis() - startTimestamp;
        model.put("duration", duration);

        return render(model, "templates/layout.vtl");
    }

    private static String renderInternalError(Exception e, String hint) {
        Map<String, Object> model = new HashMap<>();
        model.put("error_title", "Something went wront with file IO");
        model.put("error_message", hint);
        model.put("error_cause", e.getMessage());
        return render(model, "templates/layout.vtl");
    }

    private static BufferedReader getBufferedReader(
            Request req, String partName) throws IOException, ServletException {
        InputStream is = req.raw()
                            .getPart(partName)
                            .getInputStream();
        return new BufferedReader(new InputStreamReader(is));
    }

    private static String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                return cd.substring(cd.indexOf('=') + 1)
                         .trim()
                         .replace("\"", "");
            }
        }
        return null;
    }

    private static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 8080; // default port if heroku-port not set (i.e. on localhost)
    }

    private static String render(
            Map<String, Object> model, String templatePath) {
        return new VelocityTemplateEngine().render(
            new ModelAndView(model, templatePath));
    }
}