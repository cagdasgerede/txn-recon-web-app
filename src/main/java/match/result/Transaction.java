package match.result;

import match.util.DateUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import org.apache.commons.text.similarity.LevenshteinDistance;

public class Transaction {
    public static final Transaction EMPTY = new Transaction();

    /**
     * The separator used in a csv file containing the transactions.
     */
    public final static String CSV_SPLIT_BY = ",";

    // Used for computing the distance threshold for transaction narrative
    // values.
    //
    // The ratio value is taken from the following article for English being
    // a low ratio: http://wals.info/chapter/3
    private final static float LANGUAGE_CONSONANT_TO_VOWEL_RATIO = 2.5f;
    
    // Transaction fields.
    protected String
        profileName = "",
        transactionDate = "",
        transactionAmount = "",
        transactionNarrative = "",
        transactionDescription = "",
        transactionID = "",
        transactionType = "",
        walletReference = "";

    // Unparsed version of the transaction.
    protected String asString;

    // Cached version of parsed transaction date field.
    private LocalDateTime parsedTransactionDate = null;

    /**
     * The column index of each transaction field in the csv file.
     */
    public final static int
        PROFILE_NAME_INDEX = 0,
        TXN_DATE_INDEX = 1,
        TXN_AMOUNT_INDEX = 2,
        TXN_NARRATIVE_INDEX = 3,
        TXN_DESCRIPTION_INDEX = 4,
        TXN_ID_INDEX = 5,
        TXN_TYPE_INDEX = 6,
        WALLET_REFERENCE_INDEX = 7;

    /**
     * Constructs a transaction from a raw csv line.
     *
     * @param   transactionAsCSVLine
     * @return  the transaction object corresponding to the given csv line
     */
    public static Transaction build(String transactionAsCSVLine) {
        String[] parts = transactionAsCSVLine.split(CSV_SPLIT_BY);
        
        Transaction txn = new Transaction();
        if (parts.length > PROFILE_NAME_INDEX) {
            txn.profileName = parts[PROFILE_NAME_INDEX];
        }
    
        if (parts.length > TXN_DATE_INDEX) {
            txn.transactionDate = parts[TXN_DATE_INDEX];
        }
        
        if (parts.length > TXN_AMOUNT_INDEX) {
            txn.transactionAmount = parts[TXN_AMOUNT_INDEX];
        }

        if (parts.length > TXN_NARRATIVE_INDEX) {
            txn.transactionNarrative = parts[TXN_NARRATIVE_INDEX];
        }
        
        if (parts.length > TXN_DESCRIPTION_INDEX) {
            txn.transactionDescription = parts[TXN_DESCRIPTION_INDEX];
        }

        if (parts.length > TXN_ID_INDEX) {
            txn.transactionID = parts[TXN_ID_INDEX];
        }

        if (parts.length > TXN_TYPE_INDEX) {
            txn.transactionType = parts[TXN_TYPE_INDEX];
        }

        if (parts.length > WALLET_REFERENCE_INDEX) {
            txn.walletReference = parts[WALLET_REFERENCE_INDEX];
        }

        txn.asString = transactionAsCSVLine;

        return txn;
    }

    /**
     * @return profile name
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * @return transaction date
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @return transaction amount
     */
    public String getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * @return transaction narrative
     */
    public String getTransactionNarrative() {
        return transactionNarrative;
    }
    
    /**
     * @return transaction description
     */
    public String getTransactionDescription() {
        return transactionDescription;
    }

    /**
     * @return transaction id
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * @return transaction type
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @return wallet reference
     */
    public String getWalletReference() {
        return walletReference;
    }

    /**
     * @return String representation for this transaction
     */
    public String toString() {
        return asString;
    }

    /**
     * Computes the distance between this transaction and the given other
     * transaction. The distance is computed by counting the number of
     * transaction fields that need to be ignored to make both transactions
     * perfectly match.
     *
     * @param   other the transaction to align
     * @return  the distance
     */
    public int computeDistance(Transaction other)
    throws TransactionFormatException {
        int distance = 0;
        
        if (!profileName.equalsIgnoreCase(other.profileName)) {
            distance++;
        }

        if (!transactionDate.equals(other.transactionDate)) {
            // Here we try to handle the fact that the transactions might be
            // recorded in different time zones and there might be slight clock
            // differences in different recording locations.

            // Use the date objects from cached parsed copy if possible.
            if (parsedTransactionDate == null) {
                try {
                    parsedTransactionDate = DateUtil.getDate(transactionDate);
                } catch(DateTimeParseException e) {
                    throw new TransactionFormatException(
                        this.toString(),
                        "The TransactionDate format is incorrect.");
                }
            }
            if (other.parsedTransactionDate == null) {
                // Cache the parsed object.
                try {
                    other.parsedTransactionDate = DateUtil.getDate(
                        other.transactionDate);
                } catch(DateTimeParseException e) {
                    throw new TransactionFormatException(
                        other.toString(),
                        "The TransactionDate format is incorrect.");
                }
            }

            if (!DateUtil.closeEnough(parsedTransactionDate,
                                      other.parsedTransactionDate)) {
                distance++;
            }
        }
        
        if (!transactionAmount.equals(other.transactionAmount)) {
            distance++;
        }

        if (!transactionNarrative.equalsIgnoreCase(
                other.transactionNarrative)) {
            if (notSimilar(
                    transactionNarrative.toUpperCase(),
                    other.transactionNarrative.toUpperCase())) {
                distance++;
            }
        }

        if (!transactionDescription.equalsIgnoreCase(
                other.transactionDescription)) {
            if (notSimilar(
                    transactionDescription.toUpperCase(),
                    other.transactionDescription.toUpperCase())) {
                distance++;
            }
        }

        if (!transactionID.equals(other.transactionID)) {
            distance++;
        }

        if (!transactionType.equals(other.transactionType)) {
            distance++;
        }

        if (!walletReference.equals(other.walletReference)) {
            distance++;
        }

        return distance;
    }

    /**
     * "*NORTHGATE MALL GABORONE BW" and "*NRTHGT MLL GABORONE BW" can be
     * treated as being equivalent because in the second statement the words
     * are abbreviated by skipping some of the vowels. To handle this type of
     * scenarios, we can use the number of vowels as the distance threshold for
     * deciding if the statements are equivalent. For instance, in the example
     * above, the first statement has 8 vowels. The edit distance between two
     * statements is 4. Therefore, if we say our distance threshold is
     * 8, we can accurately call these two statements equivalent.
     */
    private boolean notSimilar(String source, String target) {
        int numberOfVowels =
            (int)(source.length() / LANGUAGE_CONSONANT_TO_VOWEL_RATIO + 1);
        LevenshteinDistance ldist =
            new LevenshteinDistance(numberOfVowels/* threshold */);
        return ldist.apply(source, target) == -1;
    }
}