package match.result;

import java.util.ArrayList;

/**
 * A container to keep the result of the aligning two sets of transactions.
 */
public class ReconcilationResult {
    private ArrayList<TransactionPair> approximatelyMatchingPairs;
    
    private MatchCounts fileOneCounts, fileTwoCounts;

    /**
     * Constructor.
     *
     * @param fileOneCounts match counts for the first set of transactions
     * @param fileTwoCounts match counts for the second set of transactions
     * @param approximatelyMatchingPairs list of matching transaction pairs
     */
    public ReconcilationResult(
            MatchCounts fileOneCounts,
            MatchCounts fileTwoCounts,
            ArrayList<TransactionPair> approximatelyMatchingPairs) {
        this.fileOneCounts = fileOneCounts;
        this.fileTwoCounts = fileTwoCounts;
        this.approximatelyMatchingPairs = approximatelyMatchingPairs;
    }

    /**
     * @return list of approximately mathing transaction pairs
     */
    public ArrayList<TransactionPair> getApproximatelyMatchingPairs() {
        return approximatelyMatchingPairs;
    }

    /**
     * @return match counts for the first set of transactions
     */
    public MatchCounts getFileOneCounts() {
        return fileOneCounts;
    }

    /**
     * @return match counts for the second set of transactions
     */
    public MatchCounts getFileTwoCounts() {
        return fileTwoCounts;
    }
}