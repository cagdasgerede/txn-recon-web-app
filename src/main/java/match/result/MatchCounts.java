package match.result;

/**
 * Container to keep track of the number of matching and unmatching
 * transactions.
 */
public class MatchCounts {
    private int totalTransactionCount, unmatchingTransactionCount;
    
    public MatchCounts(
            int totalTransactionCount,
            int unmatchingTransactionCount) {
        this.totalTransactionCount = totalTransactionCount;
        this.unmatchingTransactionCount = unmatchingTransactionCount;
    }

    public int getUnmatchingTransactionCount() {
        return unmatchingTransactionCount;
    }

    public int getMatchingTransactionCount() {
        return totalTransactionCount - unmatchingTransactionCount;
    }

    public int getTotalTransactionCount() {
        return totalTransactionCount;
    }
}