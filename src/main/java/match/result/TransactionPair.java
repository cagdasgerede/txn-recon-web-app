package match.result;

/**
 * A container to keep a pair of transactions.
 */
public class TransactionPair {
    private Transaction left, right;

    /**
     * Constructor.
     *
     * @param   left
     * @param   right
     */
    public TransactionPair(Transaction left, Transaction right) {
        this.left = left;
        this.right = right;
    }

    /**
     * @return  first transaction of the pair
     */
    public Transaction getLeft() { return left; }

    /**
     * @return  second transaction of the pair
     */
    public Transaction getRight() { return right; }

    /**
     * @return  Computes a String representation for the pair
     */
    public String toString() {
        return String.format("Pair:\n %s \n --- \n %s \n ===", left, right);
    }
}