package match.result;

/**
 * Class to represent errors encountered when parsing a transaction csv line
 * (for instance, when the date field value does not follow the expected
 * format).
 */
public class TransactionFormatException extends Exception {
    private String badlyFormattedData;
    private String errorMessage;

    public TransactionFormatException(
            String badlyFormattedData, String errorMessage) {
        this.badlyFormattedData = badlyFormattedData;
        this.errorMessage = errorMessage;
    }

    public String getBadlyFormattedData() {
        return badlyFormattedData;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}