package match.util;

import match.result.Transaction;
import match.result.TransactionFormatException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class IOUtil {
    /**
     * Reads transactions from a BufferedReader.
     *
     * @param   reader BufferedReader
     * @return  transactions
     */
    public static ArrayList<String> readTransactions(
            BufferedReader reader)
    throws IOException, TransactionFormatException {
        int transactionCount = 0;
        ArrayList<String> transactions = new ArrayList();
        String line = reader.readLine();
        verifyHeader(line);
        while ((line = reader.readLine()) != null) {
            transactions.add(line);
        }

        return transactions;
    }

    /**
     * Reads transactions from a file path.
     *
     * @param   filePath the path of the file to read the transactions from
     * @return transactions
     */
    public static ArrayList<String> readTransactions(String filePath)
    throws IOException, TransactionFormatException {
        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            return readTransactions(br);
        }
    }

    private static void verifyHeader(String line)
    throws TransactionFormatException {
        String[] parts = line.split(Transaction.CSV_SPLIT_BY);
        if (parts.length != 8) {
            throw new TransactionFormatException(
                line,
                "First line in the csv file must be the header line." +
                    "All expected headers could not be found." +
                    "Expected headers are:" +
                    "ProfileName, TransactionDate, TransactionAmount," +
                    "TransactionNarrative, TransactionDescription," +
                    "TransactionID, WalletReference");
        }

        boolean allHeadersAvailable =
            parts[Transaction.PROFILE_NAME_INDEX]
                .equalsIgnoreCase("ProfileName") &&
            parts[Transaction.TXN_DATE_INDEX]
                .equalsIgnoreCase("TransactionDate") &&
            parts[Transaction.TXN_AMOUNT_INDEX]
                .equalsIgnoreCase("TransactionAmount") &&
            parts[Transaction.TXN_NARRATIVE_INDEX]
                .equalsIgnoreCase("TransactionNarrative") &&
            parts[Transaction.TXN_DESCRIPTION_INDEX]
                .equalsIgnoreCase("TransactionDescription") &&
            parts[Transaction.TXN_ID_INDEX]
                .equalsIgnoreCase("TransactionID") &&
            parts[Transaction.TXN_TYPE_INDEX]
                .equalsIgnoreCase("TransactionType") &&
            parts[Transaction.WALLET_REFERENCE_INDEX]
                .equalsIgnoreCase("WalletReference");
        
        if (!allHeadersAvailable) {
            throw new TransactionFormatException(
                line,
                "First line in the csv file must be the header line." +
                    "All expected headers are not available");
        }
    }
}