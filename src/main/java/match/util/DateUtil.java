package match.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateUtil {
    private final static int ONE_HOUR_IN_SECONDS = 3600;

    // Two dates are far away from each other if the time difference is
    // within a certain range. The range begin and end are defined in
    // the following constants. Either the difference should be small enough
    // to handle small timing differences in recording the transaction time,
    // it should be almost a multiple of whole hours to capture for recording
    // locations being in different time zones.
    private final static int ALLOWED_DELTA_IN_SECONDS = 120;
    private final static int BAD_INTERVAL_BEGIN =
        ALLOWED_DELTA_IN_SECONDS;
    private final static int BAD_INTERVAL_END =
        ONE_HOUR_IN_SECONDS - ALLOWED_DELTA_IN_SECONDS;

    // Expected format of a transaction date.
    private final static String TRANSACTION_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * The method attempts to handle the time differences that arise due to
     * different time zones or clight clock variations. For the former, very
     * small differences are ignored. For the latter, a difference of multiple
     * whole numbers of hours is ignored.
     *
     * @param timeOne first time
     * @param timeTwo second time
     * @return true if times can be considered close enough
     */
    public static boolean closeEnough(
            LocalDateTime timeOne, LocalDateTime timeTwo) {
        long secDifference = Math.abs(
            ChronoUnit.SECONDS.between(timeOne, timeTwo));
        int hours = (int) (secDifference / ONE_HOUR_IN_SECONDS);

        if (hours > 24) {  // more than a day and one hour away
            return false;
        }

        long remainderSec = secDifference % ONE_HOUR_IN_SECONDS;
        if (BAD_INTERVAL_BEGIN < remainderSec &&
            remainderSec < BAD_INTERVAL_END) {
            return false;
        }

        return true;
    }

    /**
     * Utility function to create a LocalDateTime object from given String.
     *
     * @param   input raw time (for the valid format see
     *              TRANSACTION_DATE_FORMAT)
     * @return  parsed time
     */
    public static LocalDateTime getDate(String input) {
        LocalDateTime time;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
            TRANSACTION_DATE_FORMAT);
        time = LocalDateTime.parse(input, formatter);
        return time;
    }
}