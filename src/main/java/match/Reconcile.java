package match;

import match.result.MatchCounts;
import match.result.ReconcilationResult;
import match.result.Transaction;
import match.result.TransactionFormatException;
import match.result.TransactionPair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Reconcile {

    /**
     * Alings two lists of transactions. Three types of alignment are performed.
     * The first one is the perfect alignment. By this, we mean two transactions
     * contain exactly the same field values. The second type of alignment is
     * the near perfect alignment. For some transaction fields such as the
     * narrative and the date, if the differences are considered "minor", then
     * we ignore these differences. The definition of "minor" vary by field.
     * Check the Transaction class' computeDistance method for more details.
     *
     * The third type of alignment is called approximate alignment. By this, we
     * mean two transactions are exactly the same if some transaction fields are
     * completely ignored. The number of how many fields can be ignored is a
     * parameter (distanceThreshold).
     * 
     * <p>
     * The reconcilation process counts the transactions that have perfect or
     * nearly perfect matches and it computes the transaction pairs that
     * approximately aligns. If a transaction does not align at all with any
     * other transaction, then this is also reflected as a pair such as
     * (transaction, empty).
     *
     *
     * @param   txnsA list of raw transactions as csv lines
     * @param   txnsB list of raw transactions as csv lines
     * @return  the result of aligning two transaction sets
     */
    public static ReconcilationResult run(
            ArrayList<String> txnsA,
            ArrayList<String> txnsB,
            int distanceThreshold)
    throws TransactionFormatException {
        // Step 1: Eliminate perfectly matching transactions.
        ArrayList<Transaction> notPerfectlyMatchingTxnsFromA =
            getNotPerfectlyMatchingTransactions(txnsA, txnsB);
        ArrayList<Transaction> notPerfectlyMatchingTxnsFromB =
            getNotPerfectlyMatchingTransactions(txnsB, txnsA);

        // Step 2: Eliminate almost perfectly matching transaction.
        //
        // For this we use some heuristics to call two transactions equivalent.
        // For instance, we compute the edit distance for the narrative and
        // description fields to handle variations due to
        // simple typos and abbreviations. We also handle variations in
        // transaction time due to clock variations or timezone differences.
        ArrayList<Transaction> notCloselyMatchingTxnsFromA =
            getNotCloselyMatchingTransactions(
                notPerfectlyMatchingTxnsFromA,
                notPerfectlyMatchingTxnsFromB);
        ArrayList<Transaction> notCloselyMatchingTxnsFromB =
            getNotCloselyMatchingTransactions(
                notPerfectlyMatchingTxnsFromB,
                notPerfectlyMatchingTxnsFromA);

        // Step 3: Align the remaining transactions for human inspection.
        ArrayList<TransactionPair> approximateMatches =
            getApproximateMatches(
                notCloselyMatchingTxnsFromA,
                notCloselyMatchingTxnsFromB,
                distanceThreshold);

        // Step 4: Prepare the result for the user.
        MatchCounts countsForA = new MatchCounts(
            txnsA.size(),
            notCloselyMatchingTxnsFromA.size());
        MatchCounts countsForB = new MatchCounts(
            txnsB.size(),
            notCloselyMatchingTxnsFromB.size());
        return new ReconcilationResult(
            countsForA, countsForB, approximateMatches);
    }

    /**
     * Computes the list of of transactions from the first list of transactions
     * for each of which there is no perfectly matching transaction in the
     * second list. By perfect matching, we mean two transactions are exactly
     * the same.
     *
     * @param   sourceTxns transactions we look for in targetTxns
     * @param   targetTxns transactions we use to look for sourceTxns
     * @return  list of transactions from sourceTxns we could not align with any
     *              transactions from targetTxns
     */
    private static ArrayList<Transaction> getNotPerfectlyMatchingTransactions(
            ArrayList<String> sourceTxns,
            ArrayList<String> targetTxns) {
        ArrayList<Transaction> nonmatchingTxns = new ArrayList<Transaction>();

        HashSet<String> targetTxnsSet = new HashSet<String>();
        for (String targetTxn : targetTxns) {
            targetTxnsSet.add(targetTxn);
        }

        for (String sourceTxn : sourceTxns) {
            if (!targetTxnsSet.contains(sourceTxn)) {
                Transaction txn = Transaction.build(sourceTxn);
                nonmatchingTxns.add(txn);
            }
        }
        
        return nonmatchingTxns;
    }
    
    /**
     * Computes the list of of transactions from the first list of transactions
     * for each of which there is no "almost" perfectly matching transaction in
     * the second list. For details of what "almost" means, check Transaction
     * class' computeDistance function.
     *
     * @param   sourceTxns transactions we look for in targetTxns
     * @param   targetTxns transactions we use to look for sourceTxns
     * @return  list of transactions from sourceTxns we could not align with any
     *              transactions from targetTxns
     */
    private static ArrayList<Transaction> getNotCloselyMatchingTransactions(
            ArrayList<Transaction> sourceTxns,
            ArrayList<Transaction> targetTxns)
    throws TransactionFormatException {
        ArrayList<Transaction> notCloselyMatchingTxns =
            new ArrayList<Transaction>();
        
        for (Transaction sourceTxn : sourceTxns) {
            boolean thereIsMatch = false;
            for (Transaction targetTxn : targetTxns) {
                int distance = sourceTxn.computeDistance(targetTxn);
                if (distance == 0) {
                    thereIsMatch = true;
                }
            }

            if (!thereIsMatch) {
                notCloselyMatchingTxns.add(sourceTxn);                
            }
        }
        
        return notCloselyMatchingTxns;
    }

    /**
     * Computes transaction pairs that approximately match. By approximate
     * matching, we mean two transactions are exactly the same if some
     * transaction fields are totally ignored. The number of how many fields
     * can be ignored is a parameter (distanceThreshold). The goal of this
     * method to prepare transaction pairs for human inspection.
     *
     * <p>
     * If a transaction from the sourceTxns cannot be matched to any transaction
     * in targetTxns, then the pair (transaction, empty) is added to the result.
     * Similarly for a transaction from the targetTxns, the result contains
     * the pair (empty, transaction).
     *
     * @param   sourceTxns list of transactions
     * @param   targetTxns list of transactions
     * @return  list of approximately matching transaction pairs
     */
    private static ArrayList<TransactionPair> getApproximateMatches(
            ArrayList<Transaction> sourceTxns,
            ArrayList<Transaction> targetTxns,
            int distanceThreshold)
    throws TransactionFormatException {
        ArrayList<TransactionPair> pairs = new ArrayList<TransactionPair>();
        Set<Transaction> pairedTargetTxns = new HashSet<Transaction>();

        for (Transaction sourceTxn : sourceTxns) {
            boolean sourceTxnPaired = false;

            for (Transaction targetTxn : targetTxns) {
                int distance = sourceTxn.computeDistance(targetTxn);
                if (distance <= distanceThreshold) {
                    pairs.add(new TransactionPair(sourceTxn, targetTxn));    
                    sourceTxnPaired = true;
                    pairedTargetTxns.add(targetTxn);
                }
            }

            if (!sourceTxnPaired) {
                pairs.add(new TransactionPair(sourceTxn, Transaction.EMPTY));
            }
        }
        
        for (Transaction targetTxn : targetTxns) {
            if (!pairedTargetTxns.contains(targetTxn)) {
                pairs.add(new TransactionPair(Transaction.EMPTY, targetTxn));
            }
        }

        return pairs;
    }
}